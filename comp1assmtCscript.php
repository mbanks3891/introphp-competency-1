
<?php
require_once'comp1functions.php';
writeHead("Desired Comp 1.1: Artists and Albums");

//multidimensional array
$multiArray = 
array(
"The Beatles"=>array("A Hard Day's Night"=>1964,"Help!"=>1965,
"Rubber Soul"=>1965,"Abbey Road"=>1969),

"Led Zeppelin"=>array("Led Zeppelin"=>1971),

"Rolling Stones"=>array("Let it Bleed"=>1969, "Sticky Fingers"=>1971),

"The Who"=>array("Tommy"=>1969, "Quadrophenia"=>1973, 
"The Who by the Numbers"=>1975),
);


echo "<p><i>Tommy</i> by The Who was released in ".
$multiArray['The Who']['Tommy']. ".";


echo"<p><h2>All Album info:</h2>";
foreach($multiArray as $artist=>$album){
echo "<p>";
echo "<strong> $artist:</strong><br>";
foreach($album as $title=>$year){
echo "<i>$title</i><br>";
}
}

echo"<br><h2>The Who Album Release Dates :</h2>";
foreach($multiArray as $artist=>$album){
if($artist=="The Who"){
foreach($album as $title=>$year){
echo "<i>$year $title</i><br>";
}
}
}

echo"<br><h2>Albums released after 1970 :</h2>";
foreach($multiArray as $artist=>$album){
foreach($album as $title=>$year){
if($year > 1970){	
echo "$artist- <i>$title</i>, $year <br>";
}
}
}

echo"<br><br>";
writeFoot("1C");

?>



















<?php

require_once'comp1functions.php';


writeHead("PHP Comp 1.11-1.12");

$products=
array(
"t-shirt"=>array("single"=>9.99,"10 or more"=>8.99,"25 or more"=>7.99,"50 or more"=>6.99,"100 or more"=>5.99),
"ballcap"=>array("single"=>11.99,"10 or more"=>10.99,"25 or more"=>9.99,"50 or more"=>8.99,"100 or more"=>7.99),
"visor"=>array("single"=>8.99,"10 or more"=>8.49,"25 or more"=>7.99,"50 or more"=>7.49,"100 or more"=>6.99),
"magnet"=>array("single"=>4.50,"10 or more"=>4.25,"25 or more"=>4.00,"50 or more"=>3.50,"100 or more"=>3.00),
"coffee mug"=>array("single"=>6.50,"10 or more"=>6.00,"25 or more"=>5.50,"50 or more"=>5.00,"100 or more"=>4.50),
);

//show price for 25 or more visors
echo"<p>Price for 25 or more visors: \$".$products['visor']['25 or more']."</p>";

//show single prices for each item
echo"<p><strong>Single Prices:</strong>";

//loop through products array, echo the item and the price array item with index 'single'
foreach($products as $item=>$prices){
echo"<br>".$item.":\$".$prices['single'];}
echo "</p>";


echo "<p><strong>Ballcap Prices:</strong>";
//loop through products arraw row with the key 'ballcap'
FOREACH($products['ballcap'] as $qty=>$price){echo"<br>".$qty.": \$".$price;}
echo "</p>";

$maxPrice = 7.50;

echo"<p><strong>Items available for less than \$$maxPrice:</strong>";

foreach($products as $item=>$prices)
{foreach($prices as $qty=>$discPrice)
{if($discPrice < $maxPrice){echo"<br>$item -$qty: \$$discPrice";}
}}

echo '<a href="../comp1-4main.php">Main</a>';


writeFoot(1.13);

?>


















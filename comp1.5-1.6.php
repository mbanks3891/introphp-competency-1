<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<title>Lab Practice 1.5, 1.6</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0">
</head>

<body>

<div>
<header>
<h1>Lab Practice Example 1.5, 1.6</h1>
</header>


<div>
<?php
//create variables
$favColor="teal";
$favNumber="17";
//perform calculations
$addNum = $favNumber + 10;
$subNum = $favNumber - 10;
$multNum = $favNumber * 10;
$divNum = $favNumber / 10;
$modNum = $favNumber % 10;
//write out results
echo "<p>My favorite color is $favColor</p>";
echo "<p>My favorite number is $favNumber</p>";
echo "<p>Calculations using $favNumber and 10:";
echo "<br>Add: " .$addNum;
echo "<br>Subtract: " .$subNum;
echo "<br>Multiply " .$multNum;
echo "<br>Divide: " .$divNum;
echo "<br>Modulus: " .$modNum;
?>
</div>

<footer>
<p>ITSE 1406 - Competency 1</p>
<a href="../comp1-4main.php#1.5">Main</a>
</footer>

</div>
</body>
</html>



















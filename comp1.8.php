<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<title>Lab Practice </title>
<meta name="viewport" content="width=device-width; initial-scale=1.0">
</head>

<body>

<div>
<header>
<h1>Lab Practice Example </h1>
</header>


<div><?php
//************START PHP CODE

//create variable
$price = 100;

//1. FOR LOOP------------------------------
//print title
echo "<p>1. For Loop: Discount price by Week | Displaying 8 Weeks with 10% discount each week:";
echo "<br>'price' starts as $price";
echo "<br>'discountedPrice' starts as uninitialized";
echo "<br>'x' is initialized as 0 in loop statement";
echo "<br>";
// for loop using $x as a counter.  initialize to 0, continue until x reaches 8,
// add 1 to x each iteration
for ($x=0;$x<8;$x++){
//calculate new price based on the week by mutliplying the price by the .10 discount
//and subtracting from the price
$discountedPrice = $price - ($price * $x *.10);
//print the price after discount, rounded to two digits for currency
echo "<br>Week " .($x+1). ": \$" .round($discountedPrice, 2);}
echo "</p><br>";


//2. WHILE LOOP------------------------------
//print title
echo "<p>2. While Loop: Discount price by Week with $20 Minimum Price";
echo "<br>'price' starts as $price";
echo "<br>'discountedPrice' starts as $discountedPrice but is manually reset to 100";
echo "<br>'x' starts as $x but is manually reset to 0";
echo "<br>";
//initialize $x to 0 before loop.
$x = 0;
//set %discPrice to $price the first time, so the loop will run
$discountedPrice = $price;
//while loop using $discPrice as a test condition
while ($discountedPrice > 20){ //20 is our minimum, 30 is min to calc on
//calc discount price and print to page (same as for loop above)
$discountedPrice = $price - ($price * $x *.10);
echo "<br>Week " .($x+1). ": \$" .round($discountedPrice, 2);
//increment counter for discount calculation and week number
$x=$x+1;}
echo "</p><br>";


//3. DO WHILE LOOP------------------------------
//print title
echo "<p>3. Do While Loop: Quantity Discount:";
echo "<br>'price' starts as $price";
echo "<br>'discountedPrice' starts as $discountedPrice but is reset to 100";
echo "<br>'x' starts as $x but is manually reset to 0";
echo "<br>";
//initialize $x to 0 before loop.
$x = 0;
do{
//calculate discount price and write out based on quantity 
//(using .01 instead of .10 since we are adding 10 to $x each time)
$discountedPrice = $price - ($price * $x * .01);  //discountedPrice is 100 - 0 = 100 first run
echo "<br>Minimum quantity: " .$x. ": \$" .round($discountedPrice, 2);
//increment counter
$x=$x+10;
}
while($x <= 70);
echo "</p><br><br><br>";


//END PHP CODE************
?></div>



<footer>
<p>ITSE 1406 - Competency 1</p>
<a href="../comp1-4main.php#1.8">Main</a>
</footer>

</div>
</body>
</html>





















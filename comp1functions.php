<?php

function function1tax($pricearg, $quantityarg){
define('TAXRATE',.0825);

$taxAmtFunctionSide=
round($pricearg * $quantityarg * TAXRATE,2);

return $taxAmtFunctionSide;
}


function priceCalc($priceArg,$quantityArg){
$discountArray=array(0,0,.05,.1,.2,.25);//no 1.5 per instructions
if ($quantityArg > 5){$quantityArg=5;}
$discountPrice=$priceArg-($priceArg*$discountArray[$quantityArg]);
$total=$discountPrice*$quantityArg;
return $total;
}

function calcDiscount($qtyarg, $pricearg){
$discount=array(0,0,.05,.10,.15,.20,.25,.30,.35,.40,.45);
//0 items no disc, 1 item no disc, 2 items 5pct disc, etc
$discountPrice=$pricearg-($pricearg*$discount[$qtyarg]);
//$discount[$qtyarg] indicates array index
$discountedTotalFunctionSide=$discountPrice*$qtyarg;
return $discountedTotalFunctionSide;
}


function writehead($titlearg){

$headText=<<<EOD
<html lang="en">
<head>
<meta charset="utf-8">
<title>$titlearg</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0">
</head>
<body>

<div>
<header>
<h1>$titlearg</h1>
</header>

<nav>
<p>
<a href="/">Home</a> | 
<a href="about.php">About Us</a> | 
<a href="products.php">Products</a> | 
<a href="contact.php">Contact Us</a>
</p>
</nav>
EOD;

echo $headText;
}


function writefootNoArgs(){
$footText=<<<EOD
<footer>
<p>
ITSE 1406 - Competency 1 
<br>2-9-19
<a href="../comp1-4main.php#$jumparg">Main</a>
</p>
</footer>

</div>

</body>

</html>
EOD;

echo $footText;
}



function writefoot($jumparg){
$footText=<<<EOD
<footer>

<br>
<br>
<p>
ITSE 1406 - Competency 1 
<br>
for Brookhaven College
<br>
<br>
&copy Copyright by Michael Banks, 2019
<br>
<br>
<a href="../comp1-4main.php#$jumparg">Main</a>
</p>
</footer>

</div>

</body>

</html>
EOD;

echo $footText;
}


?>
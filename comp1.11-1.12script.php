<?php
require_once'comp1functions.php';


writeHead("PHP Comp 1.11-1.12");

//associative array
$menuItemsArray=array("Hamburger"=>1.99,"Cheeseburger"=>2.49,
"Fries"=>.99, "Soda"=>1.29,"Shake"=>1.79);


echo"<p><strong>Price for 1:</strong>";
foreach($menuItemsArray as $itemKeyElem=>$priceValElem){
$discountedTotalScriptSide=round(calcDiscount(1,$priceValElem),2);
//element 1 in discount array bc qty is 1
//also plug in price to apply to discount pct
//round the result to 2 decmial places
echo"<br>$itemKeyElem: \$$discountedTotalScriptSide";
}
echo"</p>";


echo"<p><strong>Price for 5:</strong>";
foreach($menuItemsArray as $itemKeyElem=>$priceValElem){
$discountedTotalScriptSide=round(calcDiscount(5,$priceValElem),2);
//element 5 in discount array bc qty is 5
//also plug in price to apply to discount pct
//round the result to 2 decmial places
echo"<br>$itemKeyElem: \$$discountedTotalScriptSide";
}
echo"</p>";



//calc an order of 5 hamburgers, 1 cheeseburger,
//3 fries, 2 sodas, and 2 shakes
echo"<br><strong>Your Order:</strong>";
//reset this
$discountedTotalScriptSide=0;


$itemTotal=round(CalcDiscount(5,$menuItemsArray['Hamburger']),2);
//using calcDiscount function:
//5 hamburgers, element 5's contents in discount array as param 1
//price of 'Hamburger' from menuItemsArray as param 2
echo"<br>5 Hamburgers: \$$itemTotal";
//accumulator
$discountedTotalScriptSide=$discountedTotalScriptSide+$itemTotal;


$itemTotal=round(CalcDiscount(3,$menuItemsArray['Cheeseburger']),2);
//using calcDiscount function:
//3 cheesburger, element 3's contents in discount array
//price of 'Cheeseburger' from menuItemsArray as param 2
echo"<br>3 Cheeseburgers: \$$itemTotal";
//accumulator
$discountedTotalScriptSide=$discountedTotalScriptSide+$itemTotal;


$itemTotal=round(CalcDiscount(3,$menuItemsArray['Fries']),2);
//using calcDiscount function:
//3 fries, element 3's contents in discount array
//price of 'Fries' from menuItemsArray as param 2
echo"<br>3 Fries: \$$itemTotal";
//accumulator
$discountedTotalScriptSide=$discountedTotalScriptSide+$itemTotal;


$itemTotal=round(CalcDiscount(2,$menuItemsArray['Soda']),2);
//using calcDiscount function:
//2 fries, element 2's contents in discount array
//price of 'Fries' from menuItemsArray as param 2
echo"<br>2 Sodas: \$$itemTotal";
//accumulator
$discountedTotalScriptSide=$discountedTotalScriptSide+$itemTotal;


$itemTotal=round(CalcDiscount(2,$menuItemsArray['Shake']),2);
//using calcDiscount function:
////2 shakes, element 2's contents in discount array
//price of 'Shake' from menuItemsArray as param 2
echo"<br>2 Shakes: \$$itemTotal";
//accumulator
$discountedTotalScriptSide=$discountedTotalScriptSide+$itemTotal;


echo"<p><strong>Total Order Price: \$$discountedTotalScriptSide</strong>";

echo"<p>----------------------------
<br>Thank you!!!</p>";

writeFoot(1.11);

?>


















